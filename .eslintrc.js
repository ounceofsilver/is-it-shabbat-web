module.exports = {
	"env": {
		"browser": true,
		"es6": true,
		"mocha": true,
	},

	"parser": "babel-eslint",

	"extends": "airbnb",

	"globals": {
		"expect": true,
		"local": true,
		"DateTime": true,
		"sinon": true,
		"proxyquire": true,
	},

	"rules": {
		"indent": ["error", "tab"],
		"no-tabs": "off",

		"react/jsx-filename-extension": [
			1,
			{ "extensions": [".js", ".jsx"] }
		],

		// Indentation
		"react/jsx-indent-props": 0,
		"react/jsx-indent": 0,

		// Until hooks are out, we use state directly
		"react/prefer-stateless-function": "off",

		// Next.js exception for pages
		"react/react-in-jsx-scope": 0,
	}
};
