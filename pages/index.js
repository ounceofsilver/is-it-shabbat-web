import { Component } from 'react';
import { Provider } from 'react-redux';

import { state } from 'is-it-shabbat-core';

import IsItShabbat from '../components/IsItShabbat';
import initialize from '../initialize';

class App extends Component {
	state = {
		isReady: false,
	};

	componentDidMount() {
		initialize()
			.then(() => this.setState({ isReady: true }));
	}

	render() {
		const { isReady } = this.state;
		return isReady
			&& (
				<Provider store={state}>
					<IsItShabbat />
				</Provider>
			);
	}
}

export default App;
