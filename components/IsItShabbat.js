import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
	localization,
	components,
	action,
	utilities,
} from 'is-it-shabbat-core';

import {
	underAWeek,
} from '../utilities/durationFormatter';

const { ShabbatCheck, CountDown } = components;
const { en: { translate } } = localization;
const { DateTime } = utilities;


function IsItShabbat(props) {
	const { now, location, dispatch } = props;
	return (
		<ShabbatCheck now={now} location={location}>
			{(period, countDownTo) => (
				<div>
					<h1>
						{`${translate.status[period]}`}
					</h1>
					<CountDown
						end={countDownTo}
						start={now}
						callback={end => dispatch(action.setNow(end))}
					>
						{dur => (
							<h2>
								{underAWeek(dur)}
							</h2>
						)}
					</CountDown>
					<h2>
						{translate.endEventName[period]}
					</h2>
				</div>
			)}
		</ShabbatCheck>
	);
}
IsItShabbat.propTypes = {
	now: PropTypes.instanceOf(DateTime).isRequired,
	location: PropTypes.shape({
		coords: PropTypes.shape({
			latitude: PropTypes.number,
			longitude: PropTypes.number,
		}),
	}).isRequired,
	dispatch: PropTypes.func.isRequired,
};

export default connect(
	state => ({
		now: state.now,
		location: state.location,
	}),
)(IsItShabbat);
