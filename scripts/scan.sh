sonar-scanner \
	  -Dsonar.projectKey=ounceofsilver_is-it-shabbat-web \
	-Dsonar.organization=ounceofsilver \
	-Dsonar.sources=. \
	-Dsonar.host.url=https://sonarcloud.io \
	-Dsonar.login=$SONARCLOUD_ISITSHABBATWEB \
	-Dsonar.javascript.lcov.reportPaths=coverage/lcov.info \
	-Dsonar.coverage.exclusions=**/*.spec.js,test/*
